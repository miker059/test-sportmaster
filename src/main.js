import './_bootstrap'
import Vue from 'vue'
import App from './App.vue'
import store from './store'

//icons
import Icon from 'vue-awesome/components/Icon.vue'
import 'vue-awesome/icons/search'
import 'vue-awesome/icons/times'

import clickOutside from './directives/click-outside'

//styles
import './scss/style.scss'

Vue.config.productionTip = false

Vue.component('icon', Icon)
Vue.directive('click-outside', clickOutside)

new Vue({
    store,
    render: h => h(App)
}).$mount('#app')
