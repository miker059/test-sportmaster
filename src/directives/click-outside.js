export default {
    bind: function (el, binding, vNode) {
        if (typeof binding.value !== 'function') {
            const compName = vNode.context.name
            let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
            if (compName) {
                warn += `Found in component '${compName}'`
            }
            console.warn(warn)
        }

        const bubble = binding.modifiers.bubble
        const handler = (e) => {
            if (!document.body.contains(e.target)) {
                return
            }
            if (bubble || (el !== e.target && !el.contains(e.target))) {
                binding.value(e)
            }
        }
        el.__vueClickOutside__ = handler

        let event = 'click'

        function addEvent (obj, type, fn) {
            if (obj.attachEvent) {
                obj['e' + type + fn] = fn
                obj[type + fn] = function () {
                    obj['e' + type + fn](window.event)
                }
                obj.attachEvent('on' + type, obj[type + fn])
            } else {
                obj.addEventListener(type, fn, false)
            }
        }

        addEvent(document, event, handler)
    },

    unbind: function (el, binding) {
        let event = 'click'

        function removeEvent (obj, type, fn) {
            if (obj.detachEvent) {
                obj.detachEvent('on' + type, obj[type + fn])
                obj[type + fn] = null
            } else {
                obj.removeEventListener(type, fn, false)
            }
        }

        removeEvent(document, event, el.__vueClickOutside__);
        el.__vueClickOutside__ = null;
    }
}

