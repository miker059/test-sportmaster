import { get } from 'axios'

/**
 * Query handler
 * @param commit
 * @param state
 * @param dispatch
 * @param value
 * @returns {Promise<void>}
 */
const queryHandler = ({ commit, state, dispatch }, value) => {
    if (value.length && !state.selectedCityId) {
        commit('SET_LOADING', true)
        dispatch('requestSuggestions', value)
    } else {
        requestSuggestions.cancel();
        commit('SET_LOADING', false)
        commit('SET_CITIES', [])
    }
}

/**
 * Request suggestions
 * @param commit
 * @param state
 * @returns {Promise<void>}
 */
const requestSuggestions = _.debounce( async ({ commit, state }, value) => {
    let { data } = await get(state.api + value, {})
    if (!data.length) {
        data.push({
            name: 'Ничего не  найдено...'
        })
    }
    commit('SET_LOADING', false)
    commit('SET_CITIES', data)
}, 800)

/**
 * Save selected city ID to state
 * @param commit
 * @param cityId
 */
const setSelectedCity = ({ commit }, cityId) => {
    commit('SET_CITY_ID', cityId)
}

export default {
    queryHandler,
    setSelectedCity,
    requestSuggestions
}
