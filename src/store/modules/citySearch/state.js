const STATE = {
    api: 'https://cors-anywhere.herokuapp.com/https://www.sportmaster.ru/rest/v1/address?query=',
    // Пришлось использовать прокси,
    // так как Апи ни при каких условиях
    // не хочет возвращать в OPTIONS
    // заголовок Access-Control-Allow-Origin,
    // пишет что он не установлен, в вызове были
    // применены условия для получения этого заголовка
    cities: [],
    selectedCityId: null,
    loading: false,
}

export default STATE
