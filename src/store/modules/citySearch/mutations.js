export default {
    /**
     * Set found cities array
     * @param state
     * @param cities
     * @constructor
     */
    SET_CITIES (state, cities) {
        state.cities = cities
    },

    /**
     * Set selected city ID
     * @param state
     * @param cityId
     * @constructor
     */
    SET_CITY_ID (state, cityId) {
        state.selectedCityId = cityId
    },

    /**
     * Set loading
     * @param state
     * @param value
     * @constructor
     */
    SET_LOADING (state, value) {
        state.loading = value
    }
}
