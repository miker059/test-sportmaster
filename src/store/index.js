import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import citySearch from './modules/citySearch'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {},
    actions: {},
    mutations: {},
    modules: {
        citySearch
    }
})
